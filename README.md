# Attribute Splash Pages

Creates category like Splash pages for your product attributes. Includes SEO URL's & layered navigation

Free[Install Now](https://my.magecloud.net/marketplace/extension/attribute-splash-pages/#popup-with-ext-form)

Compatible with Magento 1.x

-   [Overview](https://my.magecloud.net/marketplace/extension/attribute-splash-pages/#description)
-   [Reviews](https://my.magecloud.net/marketplace/extension/attribute-splash-pages/#reviews)
-   [Developer Info](https://my.magecloud.net/marketplace/developer/FishPig/)

## Attribute Splash Pages

[Attribute Splash Pages](http://fishpig.co.uk/magento/extensions/attribute-splash-pages/ "Shop by Attribute")  allows you to create splash - or landing - pages for your product attributes. Each splash page has it's own SEO URL, custom META, product list, layered navigation and is manageable in the same way as a category is. If you have already set up your attributes, setting up splash pages takes less than a minute and can impact your SEO straight away.

## Shop by Attribute Features

1.  100% free to use and easy to install via Magento Connect
2.  Allows creation of landing pages for manufacturers, colours, brands, sizes or any other attributes you have
3.  Each splash page has custom META and it's own SEO URL that you can change using the simple administration interface
4.  Uses canonical links for each Splash page
5.  Integrates with Magento's layered navigation, automatically adding splash page links (this feature can be disbaled in the config)
6.  Integrates with the product attribute block, automatically adding splash page links (this feature can be disabled in the config)
7.  Supports multi-store; create different splash pages for different stores
8.  Integrates with Magento's breadcrumbs
9.  Easy to customise via the extension's configuration area
10.  Set custom layout XML per splash page